import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import cv2
import pandas as pd
from shapely.wkt import loads as wkt_loads
import tifffile as tiff
import os
import random
from keras.models import Model, load_model
from keras.layers import Input, merge, Convolution2D, MaxPooling2D, UpSampling2D, Reshape, core, Dropout
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import backend as K
from sklearn.metrics import jaccard_similarity_score
from shapely.geometry import MultiPolygon, Polygon
import shapely.wkt
import shapely.affinity
from collections import defaultdict
from PIL import Image
from scipy.misc import toimage, imsave
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.image


dir_path = os.path.dirname(os.path.realpath(__file__))



ISZ = 160
smooth = 1e-12

N_Cls = 5

lenght=2350
width=1850

length_small = 2240
width_small = 1760

def M(image_id, t):
	print(image_id)
	if t == 0:
		print("train data transform")
		TiffImreadPath = os.path.join(dir_path, 'files/x_input_train', image_id + '.tif')

	elif t == 1:
		print("test data transform")
		TiffImreadPath = os.path.join(dir_path, 'files/x_input_test', image_id + '.tif')

	else:
		print("error")

	img = tiff.imread(TiffImreadPath.format(image_id)) #here shape is (8, 837, 849)
	# img = np.rollaxis(img, 0, 3) #here shape is (837, 849, 8)
	return img


def stretch_n(bands, lower_percent=0, higher_percent=100):
	out = np.zeros_like(bands, dtype=np.float32)
	a = 0  # np.min(band)
	b = 1  # np.max(band)
	c = np.percentile(bands[:, :], lower_percent)
	d = np.percentile(bands[:, :], higher_percent)
	t = a + (bands[:, :] - c) * (b - a) / (d - c)
	t[t < a] = a
	t[t > b] = b
	out[:, :] = t

	return out.astype(np.float32)



# t==0 for train data and t==1 for test data
def transform_img(t):
	print('Converting the masks to 1 and 0')

	if (t==0):
		print("train data transform")
		mask_path = os.path.join(dir_path, 'files/y_input_train')
	elif (t==1):
		print("test data transform")
		mask_path = os.path.join(dir_path, 'files/y_input_test')
	else:
		print("error")

	arr={}    
	for mask in os.listdir(mask_path):
		arr[mask]=(os.path.join(mask_path, mask))
	
	keylist=arr.keys()
	keylist.sort()



	for k in range(5):
		img = Image.open(os.path.join(mask_path, arr[keylist[k]]))
		print("the image ", arr[keylist[k]])
		img_array = np.array(img)

		for i in range(len(img_array)):
			for j in range(len(img_array[i])):
				if img_array[i][j] != 0:
					img_array[i][j] = 1

	 
		if (t==0):
			np.save('y_values/CNN_train/%s' %k,img_array)
			print('the file y_values/%s' %k)
		elif (t==1):
			np.save('y_values/CNN_test/%s' %k,img_array)
			print('the file y_values/%s' %k)


def stick_all_train(t):
	print("let's stick all imgs together")
	# s = lenght
	x = np.zeros((lenght, width, 4))
	y = np.zeros((lenght, width, 5))

	id = 'RGB'
	print('The id being used is: %s' %id)
	img = M(id, t)
	img = stretch_n(img)
	print(img.shape, id, np.amax(img), np.amin(img))
	# x[s * i:s * i + s, s * j:s * j + s, :] = img[:s, :s, :]
	x1 = img[:, : , :]
	print(x.shape)



	id = 'CH8'
	print('The id being used is: %s' %id)
	img = M(id, t)
	img = stretch_n(img)
	print(img.shape, id, np.amax(img), np.amin(img))
	x[:, :, 3] = img
	print(x.shape)

	x[:,:,0:2]=x1[:,:,0:2] #not sure if it works


	if t == 0:
		y_values_path = 'y_values/CNN_train'

	elif t == 1:
		y_values_path = 'y_values/CNN_test'

	else:
		print('Error')

	arr={}
	for mask in os.listdir(y_values_path):
		arr[mask]=(os.path.join(y_values_path, mask))

	keylist=arr.keys()
	keylist.sort()

	for i in range(5):
		y[:,:,i]=np.load(arr[keylist[i]])
		print("sorted test   ",(arr[keylist[i]]) )


	print(y.shape)
	print(np.amax(y), np.amin(y))

	if (t==0):
		np.save('data/CNN/x_trn', x)
		np.save('data/CNN/y_trn', y)

	elif (t==1):
		np.save('data/CNN/x_tes', x)
		np.save('data/CNN/y_tes', y)

	else:
		print('Error')



def get_patches(img, msk, amt=10000, aug=False):
	is2 = int(1.0 * ISZ)
	xm, ym = img.shape[0] - is2, img.shape[1] - is2

	x, y = [], []

	for i in range(amt):
		xc = random.randint(0, xm)
		yc = random.randint(0, ym)

		im = img[xc:xc + is2, yc:yc + is2]
		ms = msk[xc:xc + is2, yc:yc + is2]

		x.append(im)
		y.append(ms)
	x = np.array(x)
	y = np.array(y)

  #gives memory error canot fit x in memory x= 2 * x - 1

	x  = 2 * np.transpose(x, (0,3, 1, 2)) - 1
	y= np.transpose(y, (0, 3, 1, 2))
	print("x.shape, y.shape, np.amax(x), np.amin(x), np.amax(y), np.amin(y)")
	print(x.shape, y.shape, np.amax(x), np.amin(x), np.amax(y), np.amin(y))
	return x, y


def make_val(t):
	print("let's pick some samples for validation")

	if t == 0:
		img = np.load('data/CNN/x_trn.npy')
		msk = np.load('data/CNN/y_trn.npy')

	elif t == 1:
		img = np.load('data/CNN/x_tes.npy')
		msk = np.load('data/CNN/y_tes.npy')

	else:
		print('Error')

	x, y = get_patches(img, msk, amt=3000)

	if t == 0:
		np.save('data/CNN/x_tmp_train', x)
		np.save('data/CNN/y_tmp_train', y)

	elif t == 1:
		np.save('data/CNN/x_tmp_tes', x)
		np.save('data/CNN/y_tmp_tes', y)

	else:
		print('Error')




def get_unet():
	inputs = Input((4, ISZ, ISZ))# 4 insted of 3,
	conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(inputs)
	conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv1)
	pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

	conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(pool1)
	conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv2)
	pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

	conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(pool2)
	conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv3)
	pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

	conv4 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(pool3)
	conv4 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(conv4)
	pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)

	conv5 = Convolution2D(512, 3, 3, activation='relu', border_mode='same')(pool4)
	conv5 = Convolution2D(512, 3, 3, activation='relu', border_mode='same')(conv5)

	up6 = merge([UpSampling2D(size=(2, 2))(conv5), conv4], mode='concat', concat_axis=1)
	conv6 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(up6)
	conv6 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(conv6)

	up7 = merge([UpSampling2D(size=(2, 2))(conv6), conv3], mode='concat', concat_axis=1)
	conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(up7)
	conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv7)

	up8 = merge([UpSampling2D(size=(2, 2))(conv7), conv2], mode='concat', concat_axis=1)
	conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(up8)
	conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv8)

	up9 = merge([UpSampling2D(size=(2, 2))(conv8), conv1], mode='concat', concat_axis=1)
	conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(up9)
	conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv9)

	conv10 = Convolution2D(N_Cls, 1, 1, activation='sigmoid')(conv9)

	model = Model(input=inputs, output=conv10)
	model.compile(optimizer=Adam(), loss='binary_crossentropy')
	return model





def train_net():
	print("start train net")
	x_val, y_val = np.load('data/CNN/x_tmp_train.npy'), np.load('data/CNN/y_tmp_train.npy')

	img = np.load('data/CNN/x_trn.npy')
	msk = np.load('data/CNN/y_trn.npy')

	x_trn, y_trn = get_patches(img, msk)

	model = get_unet()
	model.load_weights('weights/unet_10_j5')
	model_checkpoint = ModelCheckpoint('weights/unet_tmp1.hdf5', monitor='loss', save_best_only=True)
	print("before model fit")
	model.fit(x_trn, y_trn, batch_size=64, nb_epoch=10, verbose=1, shuffle=True,
				  callbacks=[model_checkpoint], validation_data=(x_val, y_val))


	model.save_weights('weights/unet_j6')

	return model


def predict_test(model, t, trs = [0.5, 0.5, 0.5]):
	print("predict test")
	if t == 0:
		x = np.load('data/CNN/x_trn.npy')

	elif t == 1:
		x = np.load('data/CNN/x_tes.npy')

	else:
		print('Error')

	img = x
	print('The shape of X is: ', x.shape)
	cnv = np.zeros((lenght, width, 4)).astype(np.float32)
	prd = np.zeros((N_Cls, lenght, width)).astype(np.float32)
	# cnv[:x.shape[0], :x.shape[1], :] = x
	cnv = x

	# 14, 11
	for i in range(0, 14):
		line = []
		for j in range(0, 11):
			line.append(cnv[i * ISZ:(i + 1) * ISZ, j * ISZ:(j + 1) * ISZ])

		x = 2 * np.transpose(line, (0, 3, 1, 2)) - 1
		print(x.shape)
		tmp = model.predict(x, batch_size=4)
		for j in range(tmp.shape[0]):
			prd[:, i * ISZ:(i + 1) * ISZ, j * ISZ:(j + 1) * ISZ] = tmp[j]

	trs = [0.5, 0.6, 0.7, 0.7, 0.7]
	for i in range(N_Cls):
		prd[i] = prd[i] > trs[i]

	prd = prd[:, :img.shape[0], :img.shape[1]]

	np.save('11_rgb', prd)



# put 0 for real and 1 for predicted
def visualize_layers(array,t):

	if (t==0):
		name='real'
	if (t==1):
		name='pred'
	pred1 = array[:, :, 0:1]
	pred2 = array[:, :, 1:2]
	pred3 = array[:, :, 2:3]
	pred4 = array[:, :, 3:4]
	pred5 = array[:, :, 4:5]

	plt.imsave(name+'1.png', np.array(pred1).reshape(lenght, width), cmap=cm.gray)
	plt.imsave(name+'2.png', np.array(pred2).reshape(lenght, width), cmap=cm.gray)
	plt.imsave(name+'3.png', np.array(pred3).reshape(lenght, width), cmap=cm.gray)
	plt.imsave(name+'4.png', np.array(pred4).reshape(lenght, width), cmap=cm.gray)
	plt.imsave(name+'5.png', np.array(pred5).reshape(lenght, width), cmap=cm.gray)


def calculate_similarity_2d(real, pred):

	elements_real, repeats_real = np.unique(real, return_counts=True)
	elements_pred, repeats_pred = np.unique(pred, return_counts=True)
   # print("elements real", elements_real)
	print("repeats real", repeats_real)

	#print("elements pred",elements_pred)
	print("repeats predicted", repeats_pred)


	num_same_pixel = []
	for i in xrange(0,6):
		num_same_pixel.append(0)

	# plt.imsave('output/real11.png', np.array(real).reshape(length_small,width_small))
	# plt.imsave('output/pred11.png', np.array(pred).reshape(length_small,width_small))
	black_pixel_real=0
	black_pixel_pred=0

	for i in range(14*160):
		for j in range(11*160):

			if (real[i][j]==pred[i][j]==0):
				num_same_pixel[0]+=1
			if real[i][j]==pred[i][j]==1:
				num_same_pixel[1]+=1
			if real[i][j]==pred[i][j]==2:
				num_same_pixel[2]+=1
			if real[i][j]==pred[i][j]==3:
				num_same_pixel[3]+=1
			if real[i][j]==pred[i][j]==4:
				num_same_pixel[4]+=1
			if real[i][j]==pred[i][j]==5:
				num_same_pixel[5]+=1

			if real[i][j]==0:
				black_pixel_real+=1

			if pred[i][j]==0:
				black_pixel_pred+=1


	print("num_same_pixels",num_same_pixel)
	sum_same=0
	sum_real_pixel=0
	for i in xrange(1,6):
		sum_same+=num_same_pixel[i]
		sum_real_pixel+=repeats_real[i]


	print("total same pixels  ", sum_same, 'procentage of hit', float(sum_same)/sum_real_pixel)



def MultiDim_2D(x):

	y=np.zeros((length_small,width_small))
 
	for i in range(14*160):
		for j in range(11*160):

			if x[i][j][0] == 0 and x[i][j][1] == 1 and x[i][j][2] == 1 and x[i][j][3] == 1 and x[i][j][4] ==1:
				y[i][j]=1


			if x[i][j][0] == 1 and x[i][j][1] == 0 and x[i][j][2] == 1 and x[i][j][3] == 1 and x[i][j][4] ==1:
				y[i][j]=2

			if x[i][j][0] == 1 and x[i][j][1] == 1 and x[i][j][2] == 0 and x[i][j][3] == 1 and x[i][j][4] ==1:
				y[i][j]=3

			if x[i][j][0] == 1 and x[i][j][1] == 1 and x[i][j][2] == 1 and x[i][j][3] == 0 and x[i][j][4] ==1:
				y[i][j]=4

			if x[i][j][0] == 1 and x[i][j][1] == 1 and x[i][j][2] == 1 and x[i][j][3] == 1 and x[i][j][4] ==0:
				y[i][j]=5

			if x[i][j][0] == 1 and x[i][j][1] == 1 and x[i][j][2] == 1 and x[i][j][3] == 1 and x[i][j][4] ==1:
				y[i][j]=0



	return y


def visualize_rgb(array):
	new_pred = np.array(array).reshape(2240, 1760)
	rgb = np.zeros((2240, 1760, 3))

	for i in range(new_pred.shape[0]):
		for j in range(new_pred.shape[1]):
			if new_pred[i,j] == 0:
				pass

			if new_pred[i,j] == 2:
				rgb[i,j,0] = 255

			if new_pred[i,j] == 3:
				rgb[i,j,1] = 255

			if new_pred[i,j] == 1:
				rgb[i,j,2] = 255
			
			if new_pred[i,j] == 4:
				rgb[i,j,0] = 137
				rgb[i,j,1] = 0
				rgb[i,j,1] = 57

			if new_pred[i,j] == 5:
				rgb[i,j,0] = 28
				rgb[i,j,1] = 73	
				rgb[i,j,2] = 33	
	return rgb



if __name__ == '__main__':

	t = 1
	if  t == 0:
		#transform_img(t)
		#stick_all_train(t)
		#make_val(t)
		print("33")
		#train_net()
	
	elif t == 1:
		# transform_img(t)
		# stick_all_train(t)
#
		# model = get_unet()
		# model.load_weights('weights/unet_j6')
		# predict_test(model, t)

		real = np.load('data/CNN/y_tes.npy')
		pred = np.load('11_rgb.npy')
		pred = np.rollaxis(pred, 0, 3)
		print(real.shape, pred.shape)

		print(1)
		a=MultiDim_2D(real)
		b=MultiDim_2D(pred)
		print(2)
		calculate_similarity_2d(a,b)
		print(3)
		
		real = visualize_rgb(a)
		matplotlib.image.imsave('output/1real.png', real.astype(np.uint8))
		pred = visualize_rgb(b)
		matplotlib.image.imsave('output/1pred.png', pred.astype(np.uint8))

