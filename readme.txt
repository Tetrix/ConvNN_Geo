There are two Python files CNN and random_forest (RF) corresponding to the U-Net CNN algorithm and RF algorithm from the paper "Sentinel Imagery Feature Segmentation using SDI Annotated Data"
IMPORTANT: this is a prototype code, so it is not optimized or well written code. There were multiple debugging until everything worked as designed. 
The code should work without problems, but it is possible to have a need for smaller debugging. 
We have used both Anaconda and PiP  to install the environment dependences. Worked on both. 

To execute the code there is a need of manual change into the source code. 

In the CNN and RF main function the variable "t" denotes is the code executing in Training/Validation or Test mode.

The files folder contains:
The satellite image data together with the processed INSPIRE data, that from vectorized was rasterized and appropriate masks were created. 


The weights folder contains:
Pre-trained weights, and calculated weights.

Other folders are filled with processed data with the execution on CNN and RF scripts. 
It is highly advisable to first execute CNN script before RF. They are independent, but it is likely that RF script could use some CNN script pre-processed data.
Most of the functions are for data processing.  