import sys
import os
import numpy as np
import pandas as pd
from scipy.misc import toimage, imsave
import pickle as cPickle
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from PIL import Image
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.image

dir_path = os.path.dirname(os.path.realpath(__file__))



length1 = 2350
width1 = 1850

length = 2240
width = 1760


# there is no need of stretch_n because this is only needed in NN to normalize the input in the NN
# it is not need in RF


# Transforms the mask to 0 and 1
# t==0 for train data and t==1 for test data
def transform_img(t):
    print('Converting the masks to 1 and 0')
    
    if (t==0):
        print("train data transform")
        mask_path = os.path.join(dir_path, 'files/y_input_train')
    elif (t==1):
        print("test data transform")
        mask_path = os.path.join(dir_path, 'files/y_input_test')
    else:
        print("error")
        
    for mask in os.listdir(mask_path):
        img = Image.open(os.path.join(mask_path, mask))
        img_array = np.array(img)

        for i in range(len(img_array)):
            for j in range(len(img_array[i])):
                if img_array[i][j] != 0:
                    img_array[i][j] = 1


        mask = mask.replace('.tif', '')   
        if (t==0):
            np.save('y_values/RF_train/y_tr_%s' %mask, img_array)
            print('the file y_values/%s' %mask)
        elif (t==1):
            np.save('y_values/RF_test/y_te_%s' %mask, img_array)
            print('the file y_values/%s' %mask)



# combines all the masks into one array
def combine_mask_1d(t):
    
    print('combine mask 1d')
    
    if (t==0):
        print("train data mask")
        mask_path = os.path.join(dir_path, 'y_values/RF_train/')
    elif (t==1):
        print("test data mask")
        mask_path = os.path.join(dir_path, 'y_values/RF_test/')
    else:
        print("error")
    
    arr={}    
    for mask in os.listdir(mask_path):
        arr[mask]=(os.path.join(mask_path, mask))
    
    keylist=arr.keys()
    keylist.sort()
    
    y_cal = np.zeros((length1, width1,5))
    for i in range(5):
        y_cal[:,:,i]=np.load(arr[keylist[i]])
        print("sorted test   ",(arr[keylist[i]]) )
    
    y = np.zeros((length, width))
    count=np.zeros(5) 


    for i in range(length):
        for j in range(width):
           # print('i', i, 'j  ',j,' a1'  ,y_cal[i][j][0], '  b1   ', y_cal[i][j][1], '  c1  ' , y_cal[i][j][2])
            if y_cal[i][j][0] == 0 and y_cal[i][j][1] == 1 and y_cal[i][j][2] == 1 and y_cal[i][j][3] == 1 and y_cal[i][j][4] == 1:
                y[i][j] = 1
                count[0] += 1
               

            elif y_cal[i][j][0] == 1 and y_cal[i][j][1] == 0 and y_cal[i][j][2] == 1 and y_cal[i][j][3] == 1 and y_cal[i][j][4] == 1:
                y[i][j] = 2
                count[1] += 1
             
            elif y_cal[i][j][0] == 1 and y_cal[i][j][1] == 1 and y_cal[i][j][2] == 0 and y_cal[i][j][3] == 1 and y_cal[i][j][4] == 1:
                y[i][j] = 3
                count[2] += 1
            
            elif y_cal[i][j][0] == 1 and y_cal[i][j][1] == 1 and y_cal[i][j][2] == 1 and y_cal[i][j][3] == 0 and y_cal[i][j][4] == 1:
                y[i][j] = 4
                count[3] += 1    
            
            elif y_cal[i][j][0] == 1 and y_cal[i][j][1] == 1 and y_cal[i][j][2] == 1 and y_cal[i][j][3] == 1 and y_cal[i][j][4] == 0:

                y[i][j] = 5
                count[4] += 1
 
    np.save('rf_y', y)

    c = y.flatten()
    all_counted = 0
    for i in range(5):
        all_counted+=count[i]
   
    print('Difference is: ', (length * width - all_counted))
    
    if (t==0):
        print("train data saved")
        np.save(os.path.join(dir_path, 'data/RF/y_trn'), c)
    elif (t==1):
        print("test data saved")
        np.save(os.path.join(dir_path, 'data/RF/y_tes'), c)



def combine_X(t):   
    if (t==0):
        print(" X train combine")
        #img = tiff.imread(os.path.join(dir_path, 'files/train_input/CH8.tif'))
        img = Image.open(os.path.join(dir_path, 'files/x_input_train/RGB.tif'))
        img = np.array(img)
        img = img[:length, :width]

        img1 = Image.open(os.path.join(dir_path, 'files/x_input_train/CH8.tif'))
        img1 = np.array(img1)
        img1 = img1[:length, :width]
    elif (t==1):
        print("X test combine")
        img = Image.open(os.path.join(dir_path, 'files/x_input_test/RGB.tif'))
        img = np.array(img)
        img = img[:length, :width]

        img1 = Image.open(os.path.join(dir_path, 'files/x_input_test/CH8.tif'))
        img1 = np.array(img1)
        img1 = img1[:length, :width]
    else:
        print("error")
    
    x = np.zeros((length, width, 4))


    img = np.array(img)
    print(img.shape, id, np.amax(img), np.amin(img))

    x1 = img[:, : , :]
    

    img1 = np.array(img1)
    print(img.shape, id, np.amax(img), np.amin(img))
    x[:, :, 3] = img1
    
    x[:,:,0:2]=x1[:,:,0:2] #not sure if it works
    
    if (t==0):
        np.save(os.path.join(dir_path, 'data/RF/x_trn'), x)
    elif (t==1):
        np.save(os.path.join(dir_path, 'data/RF/x_tes'), x)
    
    

    

def visualize_img(array_file, output_file):
    img = np.load(array_file)
    print(img.shape)
    # img = np.rollaxis(img, 0, 3)
    imsave(output_file, img)     


def accuracy(real, pred):
    print('Calculating accuracy')
    
    real1=real
    pred1=pred
    
    print("real shape", real.shape)
    print("pred shape", pred.shape)
    
    # real=real[:length*width]
    pred=pred[:length*width]
    
    print("the new real shape", real.shape)
    print("the new pred shape", pred.shape)
    
    elements_real, repeats_real = np.unique(real, return_counts=True)
    elements_pred, repeats_pred = np.unique(pred, return_counts=True)

    print('REPEATS REALL', repeats_real)

    num_same_pixel = 0
    black_pixel_real = 0
    black_pixel_pred = 0

    num_same_pixel = []
    for i in xrange(0,6):
        num_same_pixel.append(0)   

    print(real.shape, pred.shape)
    for i in range(len(real)):
        if (real[i] == pred[i] == 0):
            num_same_pixel[0] += 1

        if real[i] == pred[i] == 1:
            num_same_pixel[1] += 1
        
        if real[i] == pred[i] == 2:
            num_same_pixel[2] += 1
        
        if real[i] == pred[i] == 3:
            num_same_pixel[3] += 1
        
        if real[i] == pred[i] == 4:
            num_same_pixel[4] += 1
        
        if real[i] == pred[i] == 5:
            num_same_pixel[5] += 1
            
        if real[i] == 0:
            black_pixel_real += 1
        
        if pred[i] == 0:
            black_pixel_pred += 1

    print("num_same_pixels",num_same_pixel)
    sum_same=0
    sum_real_pixel=0
    for i in xrange(1,6):
        sum_same += num_same_pixel[i]
        sum_real_pixel += repeats_real[i]

    print('repeat_real', repeats_real)
    print("repeat_pred", repeats_pred)
    print("total same pixels  ", sum_same, 'percentage of hit', float(sum_same)/sum_real_pixel)
    
   
    # plt.imsave('output/real_img_2d.png', np.array(real1).reshape(length1, width1), cmap=cm.gray)
    # plt.imsave('output/pred_img_2d.png', np.array(pred1).reshape(length1, width1), cmap=cm.gray)
    
    
    
    

# train random forest model
def train_random_forest(X, y):
    print("starting RF classifier")
    model = RandomForestClassifier()
    print('Making model.fit')
    fitted_model = model.fit(X, y)
    return fitted_model

# save the trained model
def save_model(fitted_model):
    with open('fitted_rf.pkl', 'wb') as fid:
        cPickle.dump(fitted_model, fid)
    print("model saved")
        

# load the trained model
def load_model():         
    with open('fitted_rf.pkl', 'rb') as fid:
        model = cPickle.load(fid) 
    return model

# predict result
def predict_result(data):
    print('Predicting the result:')
    predict = model.predict(data)
    return predict



def visualize_rgb(array):
	new_pred = np.array(array).reshape(length, width)
	rgb = np.zeros((length,width,3))

	for i in range(new_pred.shape[0]):
		for j in range(new_pred.shape[1]):
			if new_pred[i,j] == 0:
				pass

			if new_pred[i,j] == 2:
				rgb[i,j,0] = 255

			if new_pred[i,j] == 3:
				rgb[i,j,1] = 255

			if new_pred[i,j] == 1:
				rgb[i,j,2] = 255
			
			if new_pred[i,j] == 4:
				rgb[i,j,0] = 137
				rgb[i,j,1] = 0
				rgb[i,j,1] = 57

			if new_pred[i,j] == 5:
				rgb[i,j,0] = 28
				rgb[i,j,1] = 73	
				rgb[i,j,2] = 33	
	return rgb




if __name__ == '__main__':
    
    # train the RF, the argument in the function is 0
    
    t=1
    if (t==0):
        transform_img(0)
        combine_mask_1d(0)
        combine_X(0)
        # load X and y trainig
        X = np.load(os.path.join(dir_path, 'data/RF/x_trn.npy'))
        X = X.transpose(2,0,1).reshape(4,-1)
        X = np.rollaxis(X, 1, 0)
        y = np.load(os.path.join(dir_path, 'data/RF/y_trn.npy'))
        # train the classifier
        #fitted_model = train_random_forest(X, y)
        # save the model
        #save_model(fitted_model)    

    if (t==1):
        #transform_img(t)
        #combine_mask_1d(t)
        #combine_X(t)
        
        # load X and y test
        X = np.load(os.path.join(dir_path, 'data/RF/x_tes.npy'))
        X = X.transpose(2,0,1).reshape(4,-1)
        X = np.rollaxis(X, 1, 0)
        y = np.load(os.path.join(dir_path, 'data/RF/y_tes.npy'))
        
        
        
        # load the model
        #model = load_model()
        
        #predict a predict_result
        #predict = predict_result(X)
        #np.save('predict', predict)
        predict = np.load('predict.npy')
        #accuracy(y, predict)
        plt.imsave('1predict.png', np.array(predict).reshape(length, width), cmap=cm.gray)
        plt.imsave('1real.png', np.array(y).reshape(length, width), cmap=cm.gray)
        
        pred = visualize_rgb(predict)
        matplotlib.image.imsave('output/1predRF.png', pred.astype(np.uint8))

        real = visualize_rgb(y)
        matplotlib.image.imsave('output/1realRF.png', real.astype(np.uint8))




        # print(np.unique(predict, return_counts=True))
        # print(np.unique(y, return_counts=True))

        # test = np.load('rf_y.npy')

        # elements_real, repeats_real = np.unique(test, return_counts=True)

        # print('elements real:', elements_real)
        # print('elements repeat:', repeats_real)
      


   