import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import cv2
import pandas as pd
from shapely.wkt import loads as wkt_loads
import tifffile as tiff
import random
from keras.models import Model, load_model
from keras.layers import Input, merge, Convolution2D, MaxPooling2D, UpSampling2D, Reshape, core, Dropout
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import backend as K
from sklearn.metrics import jaccard_similarity_score
from shapely.geometry import MultiPolygon, Polygon
import shapely.wkt
import shapely.affinity
from collections import defaultdict
from PIL import Image
from scipy.misc import toimage, imsave


dir_path = os.path.dirname(os.path.realpath(__file__))

DFpath= os.path.join(dir_path, 'files/train_wkt_v4.csv')
GSpath= os.path.join(dir_path, 'files/grid_sizes.csv')
SBpath= os.path.join(dir_path, 'files/sample_submission.csv')
TiffImreadPath= os.path.join(dir_path, 'files/input/{}.tif')
mask_path = os.path.join(dir_path, 'files/output')


DF = pd.read_csv(DFpath)
GS = pd.read_csv(GSpath, names=['ImageId', 'Xmax', 'Ymin'], skiprows=1)
SB = pd.read_csv(SBpath)


ISZ = 160
smooth = 1e-12

N_Cls = 3

def M(image_id):
	# __author__ = amaia
	# https://www.kaggle.com/aamaia/dstl-satellite-imagery-feature-detection/rgb-using-m-bands-example
	print(image_id)
	img = tiff.imread(TiffImreadPath.format(image_id)) #here shape is (8, 837, 849)
	# img = np.rollaxis(img, 0, 3) #here shape is (837, 849, 8)
	return img

def stretch_n(bands, lower_percent=0, higher_percent=100):
	out = np.zeros_like(bands, dtype=np.float32)
	n = bands.shape[2]
	for i in range(n):
		a = 0  # np.min(band)
		b = 1  # np.max(band)
		c = np.percentile(bands[:, :, i], lower_percent)
		d = np.percentile(bands[:, :, i], higher_percent)
		t = a + (bands[:, :, i] - c) * (b - a) / (d - c)
		t[t < a] = a
		t[t > b] = b
		out[:, :, i] = t

	return out.astype(np.float32)

# change rgb pixels in the image
def change_predict_pixels(img_array):
	img_array = np.rollaxis(img_array, 0, 3)
	for i in range(len(img_array)):
		for j in range(len(img_array[i])):
			# convert green to brown
			if img_array[i][j][0] == 0 and img_array[i][j][1] == 1 and img_array[i][j][2] == 0:
				img_array[i][j] = [0.2, 0.2, 0.1]

			# convert light blue to green
			elif img_array[i][j][0] == 0 and img_array[i][j][1] == 1 and img_array[i][j][2] == 1:
				img_array[i][j] = [0, 1, 0]    
			
			# convert blue to green
			elif img_array[i][j][0] == 0 and img_array[i][j][1] == 0 and img_array[i][j][2] == 1:
				img_array[i][j] = [0, 1, 0]  
	return img_array


def change_y_pixels(img_array):
	# img_array = np.rollaxis(img_array, 0, 3)

	for i in range(len(img_array)):
		for j in range(len(img_array[i])):
	#         # convert blue to green
			if img_array[i][j][0] == 0 and img_array[i][j][1] == 0 and img_array[i][j][2] == 1:
				img_array[i][j] = [0, 1, 0]
			
	#         # convert red to green
	# 		elif img_array[i][j][0] == 1 and img_array[i][j][1] == 0 and img_array[i][j][2] == 0:
	# 			img_array[i][j] = [0, 1, 0]    
			
	# #         # convert blue to red
	# 		elif img_array[i][j][0] == 0 and img_array[i][j][1] == 0 and img_array[i][j][2] == 1:
	# 			img_array[i][j] = [1, 0, 0]
			
	return img_array	


# Transforms the mask to 0 and 1
def transform_img(mask_path):
	print('Converting the masks to 1 and 0')
	dir_path = os.path.dirname(os.path.realpath(__file__))
	if not os.path.exists(os.path.join(dir_path, 'y_values')):
		os.makedirs(os.path.join(dir_path, 'y_values'))

	for mask in os.listdir(mask_path):
		img = Image.open(os.path.join(mask_path, mask))
		img_array = np.array(img)

		for i in range(len(img_array)):
			for j in range(len(img_array[i])):
				if img_array[i][j] != 0:
					img_array[i][j] = 1
		mask = mask.replace('.tif', '')                
		np.save('y_values/y_values_%s' %mask, img_array)                

def stick_all_train():
	print("let's stick all imgs together")
	# s = 2344
	x = np.zeros((2207, 1714, 3))
	y = np.zeros((2207, 1714, 3))

	ids = sorted(DF.ImageId.unique())
	print('The number od ids is: %d' %len(ids))

	id = 'rgb1'
	print('The id being used is: %s' %id)
	img = M(id)
	img = stretch_n(img)
	print(img.shape, id, np.amax(img), np.amin(img))
	# x[s * i:s * i + s, s * j:s * j + s, :] = img[:s, :s, :]
	x = img[:, : , :]
	print(x.shape)

	z = 0
	for y_array in os.listdir('y_values'):
		print(y_array)
		y_single = np.load(os.path.join('y_values', y_array))
		print(y_single.shape)
		# print(y_single.shape)
		y[:, :, z] = y_single
		z += 1

	print(y.shape)
	print(np.amax(y), np.amin(y))
	# y = change_y_pixels(y)

	np.save('data/x_trn', x)
	np.save('data/y_trn', y)


def get_patches(img, msk, amt=10000, aug=False):
	is2 = int(1.0 * ISZ)
	xm, ym = img.shape[0] - is2, img.shape[1] - is2

	x, y = [], []

	tr = [0.4, 0.1, 0.1, 0.15, 0.3, 0.95, 0.1, 0.05, 0.001, 0.005]
	for i in range(amt):
		xc = random.randint(0, xm)
		yc = random.randint(0, ym)

		im = img[xc:xc + is2, yc:yc + is2]
		ms = msk[xc:xc + is2, yc:yc + is2]

		# for j in range(N_Cls):
		# 	sm = np.sum(ms[:, :, j])
		# 	if 1.0 * sm / is2 ** 2 > tr[j]:
		# 		if aug:
		# 			if random.uniform(0, 1) > 0.5:
		# 				im = im[::-1]
		# 				ms = ms[::-1]
						
		# 			if random.uniform(0, 1) > 0.5:
		# 				im = im[:, ::-1]
		# 				ms = ms[:, ::-1]

		# 		x.append(im)
		# 		y.append(ms)

		x.append(im)
		y.append(ms)	
	x = np.array(x)
	y = np.array(y)

	x, y = 2 * np.transpose(x, (0, 3, 1, 2)) - 1, np.transpose(y, (0, 3, 1, 2))
	print(x.shape, y.shape, np.amax(x), np.amin(x), np.amax(y), np.amin(y))
	return x, y


def make_val():
	print("let's pick some samples for validation")
	img = np.load('data/x_trn.npy')
	msk = np.load('data/y_trn.npy')
	x, y = get_patches(img, msk, amt=3000)

	np.save('data/x_tmp', x)
	np.save('data/y_tmp', y)


def jaccard_coef(y_true, y_pred):
	# __author__ = Vladimir Iglovikov
	intersection = K.sum(y_true * y_pred, axis=[0, -1, -2])
	sum_ = K.sum(y_true + y_pred, axis=[0, -1, -2])

	jac = (intersection + smooth) / (sum_ - intersection + smooth)

	return K.mean(jac)


def jaccard_coef_int(y_true, y_pred):
	# __author__ = Vladimir Iglovikov
	y_pred_pos = K.round(K.clip(y_pred, 0, 1))

	intersection = K.sum(y_true * y_pred_pos, axis=[0, -1, -2])
	sum_ = K.sum(y_true + y_pred_pos, axis=[0, -1, -2])
	jac = (intersection + smooth) / (sum_ - intersection + smooth)
	return K.mean(jac)


def get_unet():
	inputs = Input((3, ISZ, ISZ))
	conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(inputs)
	conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv1)
	pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

	conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(pool1)
	conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv2)
	pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

	conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(pool2)
	conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv3)
	pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

	conv4 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(pool3)
	conv4 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(conv4)
	pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)

	conv5 = Convolution2D(512, 3, 3, activation='relu', border_mode='same')(pool4)
	conv5 = Convolution2D(512, 3, 3, activation='relu', border_mode='same')(conv5)

	up6 = merge([UpSampling2D(size=(2, 2))(conv5), conv4], mode='concat', concat_axis=1)
	conv6 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(up6)
	conv6 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(conv6)

	up7 = merge([UpSampling2D(size=(2, 2))(conv6), conv3], mode='concat', concat_axis=1)
	conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(up7)
	conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv7)

	up8 = merge([UpSampling2D(size=(2, 2))(conv7), conv2], mode='concat', concat_axis=1)
	conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(up8)
	conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv8)

	up9 = merge([UpSampling2D(size=(2, 2))(conv8), conv1], mode='concat', concat_axis=1)
	conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(up9)
	conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv9)

	conv10 = Convolution2D(N_Cls, 1, 1, activation='sigmoid')(conv9)

	model = Model(input=inputs, output=conv10)
	model.compile(optimizer=Adam(), loss='binary_crossentropy', metrics=[jaccard_coef, jaccard_coef_int, 'accuracy'])
	return model


def calc_jacc(model):
	img = np.load('data/x_tmp.npy')
	msk = np.load('data/y_tmp.npy')

	prd = model.predict(img, batch_size=4)
	print(prd.shape, msk.shape)
	avg, trs = [], []

	for i in range(N_Cls):
		t_msk = msk[:, i, :, :]
		t_prd = prd[:, i, :, :]
		t_msk = t_msk.reshape(msk.shape[0] * msk.shape[2], msk.shape[3])
		t_prd = t_prd.reshape(msk.shape[0] * msk.shape[2], msk.shape[3])

		m, b_tr = 0, 0
		for j in range(10):
			tr = j / 10.0
			pred_binary_mask = t_prd > tr

			jk = jaccard_similarity_score(t_msk, pred_binary_mask)
			if jk > m:
				m = jk
				b_tr = tr
		print(i, m, b_tr)
		avg.append(m)
		trs.append(b_tr)

	score = sum(avg) / 10.0
	return score, trs


def train_net():
	print("start train net")
	x_val, y_val = np.load('data/x_tmp.npy'), np.load('data/y_tmp.npy')
	img = np.load('data/x_trn.npy')
	msk = np.load('data/y_trn.npy')

	x_trn, y_trn = get_patches(img, msk)

	model = get_unet()
	# model.load_weights('weights/unet_10_jk0.7878')
	# model_checkpoint = ModelCheckpoint('weights/unet_tmp.hdf5', monitor='loss', save_best_only=True)
	for i in range(1):
		model.fit(x_trn, y_trn, batch_size=64, nb_epoch=5, verbose=1, shuffle=True,
				  callbacks=[model_checkpoint], validation_data=(x_val, y_val))
		del x_trn
		del y_trn
		x_trn, y_trn = get_patches(img, msk)
		score, trs = calc_jacc(model)
		print('val jk', score)
		model.save_weights('weights/unet_10_jk%.4f' % score)

	return model


def predict_id(id, model, trs):
	img = M(id)
	x = stretch_n(img)
	cnv = np.zeros((2344, 1836, 3)).astype(np.float32)
	prd = np.zeros((N_Cls, 2344, 1836)).astype(np.float32)
	cnv[:img.shape[0], :img.shape[1], :] = x

	# 14, 11
	for i in range(0, 14):
		line = []
		for j in range(0, 11):
			line.append(cnv[i * ISZ:(i + 1) * ISZ, j * ISZ:(j + 1) * ISZ])

		x = 2 * np.transpose(line, (0, 3, 1, 2)) - 1
		print(x.shape)
		tmp = model.predict(x, batch_size=4)
		for j in range(tmp.shape[0]):
			prd[:, i * ISZ:(i + 1) * ISZ, j * ISZ:(j + 1) * ISZ] = tmp[j]

	# trs = [0.4, 0.1, 0.4]
	trs = [0.1, 0.1, 0.1]
	for i in range(N_Cls):
		prd[i] = prd[i] > trs[i]

	return prd[:, :img.shape[0], :img.shape[1]]



def predict_test(model, trs = [0.5, 0.5, 0.5]):
	print("predict test")
	# for i, id in enumerate(sorted(set(SB['ImageId'].tolist()))):
	id = 'rgb1'
	msk = predict_id(id, model, trs)
	# msk = change_predict_pixels(msk)
	np.save('msk/10_%s' % id, msk)
	# if i % 100 == 0: 
	print('Predicted id:', id)


def mask_for_polygons(polygons, im_size):
	# __author__ = Konstantin Lopuhin
	# https://www.kaggle.com/lopuhin/dstl-satellite-imagery-feature-detection/full-pipeline-demo-poly-pixels-ml-poly
	img_mask = np.zeros(im_size, np.uint8)
	if not polygons:
		return img_mask
	int_coords = lambda x: np.array(x).round().astype(np.int32)
	exteriors = [int_coords(poly.exterior.coords) for poly in polygons]
	interiors = [int_coords(pi.coords) for poly in polygons
				 for pi in poly.interiors]
	cv2.fillPoly(img_mask, exteriors, 1)
	cv2.fillPoly(img_mask, interiors, 0)
	return img_mask


def mask_to_polygons(mask, epsilon=1, min_area=1.):
	# __author__ = Konstantin Lopuhin
	# https://www.kaggle.com/lopuhin/dstl-satellite-imagery-feature-detection/full-pipeline-demo-poly-pixels-ml-poly

	# first, find contours with cv2: it's much faster than shapely
	image, contours, hierarchy = cv2.findContours(
		((mask == 1) * 255).astype(np.uint8),
		cv2.RETR_CCOMP, cv2.CHAIN_APPROX_TC89_KCOS)
	# create approximate contours to have reasonable submission size
	approx_contours = [cv2.approxPolyDP(cnt, epsilon, True)
					   for cnt in contours]
	if not contours:
		return MultiPolygon()
	# now messy stuff to associate parent and child contours
	cnt_children = defaultdict(list)
	child_contours = set()
	assert hierarchy.shape[0] == 1
	# http://docs.opencv.org/3.1.0/d9/d8b/tutorial_py_contours_hierarchy.html
	for idx, (_, _, _, parent_idx) in enumerate(hierarchy[0]):
		if parent_idx != -1:
			child_contours.add(idx)
			cnt_children[parent_idx].append(approx_contours[idx])
	# create actual polygons filtering by area (removes artifacts)
	all_polygons = []
	for idx, cnt in enumerate(approx_contours):
		if idx not in child_contours and cv2.contourArea(cnt) >= min_area:
			assert cnt.shape[1] == 1
			poly = Polygon(
				shell=cnt[:, 0, :],
				holes=[c[:, 0, :] for c in cnt_children.get(idx, [])
					   if cv2.contourArea(c) >= min_area])
			all_polygons.append(poly)
	# approximating polygons might have created invalid ones, fix them
	all_polygons = MultiPolygon(all_polygons)
	if not all_polygons.is_valid:
		all_polygons = all_polygons.buffer(0)
		# Sometimes buffer() converts a simple Multipolygon to just a Polygon,
		# need to keep it a Multi throughout
		if all_polygons.type == 'Polygon':
			all_polygons = MultiPolygon([all_polygons])
	return all_polygons    


def check_predict(id='input_RGB_f'):
	print(id)
	model = get_unet()
	model.load_weights('weights/unet_10_jk0.1958')

	msk = predict_id(id, model, [0.4, 0.1, 0.4, 0.3, 0.3, 0.5, 0.3, 0.6, 0.1, 0.1])
	img = M(id)
	print(img.shape)
	plt.figure()
	ax1 = plt.subplot(131)
	ax1.set_title('image ID:input_RGB_f')
	ax1.imshow(img[:, :, 2], cmap=plt.get_cmap('gist_ncar'))
	ax2 = plt.subplot(132)
	ax2.set_title('predict bldg pixels')
	ax2.imshow(msk[0], cmap=plt.get_cmap('gray'))
	ax3 = plt.subplot(133)
	ax3.set_title('predict bldg polygones')
	ax3.imshow(mask_for_polygons(mask_to_polygons(msk[0], epsilon=1), img.shape[:2]), cmap=plt.get_cmap('gray'))

	plt.show()  

def visualize_img(array_file, output_file):
	img = np.load(array_file)
	print(img.shape)
	# img = np.rollaxis(img, 0, 3)
	imsave(output_file, img)	  


def calculate_similarity(real, pred):
	pred = pred.astype(float)
	pred = pred.round(decimals=1)

	num_black = 0
	num_same = 0
	num_different = 0

	for i in range(len(real)):
		for j in range(len(real[i])):
			if real[i][j][0] == 0 and real[i][j][1] == 0 and real[i][j][2] == 0:
				num_black += 1		
			if (real[i][j][0] == 0 and real[i][j][1] == 0 and real[i][j][2] == 0) or (pred[i][j][0] == 0 and pred[i][j][1] == 0 and pred[i][j][2] == 0):
				pass
			else:
				if real[i][j][0] == pred[i][j][0] and real[i][j][1] == pred[i][j][1] and real[i][j][2] == pred[i][j][2]:
					num_same += 1
				else: 
					num_different += 1

	print('Black pixels: %d, Same pixels: %d, Different pixels: %d' %(num_black, num_same, num_different))

	num_same = num_same + num_black
	all_pixels = float(num_same) + float(num_different)
	correct_percentage = float(num_same) / all_pixels
	missed_percentage = float(num_different) / all_pixels

	print('The correct pixels are: %f, the missed pixels are: %f' %(correct_percentage, missed_percentage)) 


if __name__ == '__main__':
	# transform_img(mask_path)
	# stick_all_train()
	# make_val()
	# model = get_unet()
	# model.load_weights('weights/unet_10_j-4')
	# predict_test(model)
	# check_predict()
	
	# real = np.load('data/y_trn.npy')
	# y = np.load('data/y_trn.npy')
	# real = change_y_pixels(y)

	# y = np.load('data/y_trn.npy')
	# y_changed = change_y_pixels(y)
	# np.save('data/y_trn_changed.npy', y_changed)

	# pred = np.load('msk/10_rgb1.npy')
	# real = np.load('data/y_trn.npy')
	# pred = np.rollaxis(pred, 0, 3)
	# calculate_similarity(real, pred)
	# x = np.load('data/x_trn.npy')
	# print(np.unique(x))

	# array_file = os.path.join(dir_path, 'msk/10_rgb1.npy')
	# output_file = os.path.join(dir_path, 'predict.png')
	# visualize_img(array_file, output_file)

	# array_file = os.path.join(dir_path, 'data/x_trn.npy')
	# output_file = os.path.join(dir_path, 'x.png')
	# visualize_img(array_file, output_file)

	array_file = os.path.join(dir_path, 'data/y_trn.npy')
	output_file = os.path.join(dir_path, 'y.png')
	visualize_img(array_file, output_file)

	# y_true = os.path.join(dir_path, 'data/y_trn.npy')
	# y_pred = os.path.join(dir_path, 'msk/10_input_RGB_f.npy')
	# print(jaccard_coef(y_true, y_pred))

	# img = np.load(os.path.join(dir_path, 'data/x_trn.npy'))
	# print(img[0][0][0])
	# img = img.transpose(2,0,1).reshape(3,-1)
	# print(img[0][0])